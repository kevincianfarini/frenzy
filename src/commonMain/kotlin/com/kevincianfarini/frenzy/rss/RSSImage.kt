package com.kevincianfarini.frenzy.rss

class RSSImage(
    val url: String,
    val title: String,
    val link: String
)