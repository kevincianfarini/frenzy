package com.kevincianfarini.frenzy.rss

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XmlElement
import nl.adaptivity.xmlutil.serialization.XmlSerialName

@Serializable
@XmlSerialName("rss", "", "")
internal class Rss(
    val version: Double,
    @XmlSerialName("channel", "", "") val channel: Channel,
)

@Serializable internal class Channel(
    @XmlElement(true) val title: String,
    @XmlElement(true) val description: String,
    @XmlElement(true) val link: String,
    val image: Image? = null,
)

@Serializable internal class Image(
    @XmlElement(true) val url: String,
    @XmlElement(true) val title: String,
    @XmlElement(true) val link: String,
)