package com.kevincianfarini.frenzy

import com.kevincianfarini.frenzy.rss.RSSImage
import com.kevincianfarini.frenzy.rss.Rss
import kotlinx.serialization.decodeFromString
import nl.adaptivity.xmlutil.serialization.XML

sealed class Feed {
    companion object {
        fun parse(feedContent: String): Feed {
            val deserialized = XML().decodeFromString<Rss>(feedContent)
            return RSSFeed.fromSerializable(deserialized)
        }
    }
}

data class RSSFeed(
    val title: String,
    val link: String,
    val description: String,
    val language: String? = null,
    val copyright: String? = null,
    val managingEditor: String? = null,
    val webMaster: String? = null,
    val pubDate: String? = null, // todo https://github.com/Kotlin/kotlinx-datetime/issues/83
    val lastBuildDate: String? = null, // todo
    val category: String? = null,
    val generator: String? = null,
    val docs: String? = null,
    val cloud: String? = null,
    val timeToLive: String? = null, // todo
    val image: RSSImage? = null
) : Feed() {

    internal companion object {
        fun fromSerializable(serializable: Rss) = RSSFeed(
            title = serializable.channel.title,
            description = serializable.channel.description,
            link = serializable.channel.link
        )
    }
}

class AtomFeed(

) : Feed()