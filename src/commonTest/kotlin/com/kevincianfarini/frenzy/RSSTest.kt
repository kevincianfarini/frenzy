package com.kevincianfarini.frenzy

import kotlin.test.Test
import kotlin.test.assertEquals

class RSSTest {

    @Test fun `RSS feed with only required properties`() {
        val string = """
            <rss version="2.0">
              <channel>
                <title>Foo</title>
                <description>Bar</description>
                <link>https://baz.com/</link>
              </channel>
            </rss>
        """.trimIndent()

        assertEquals(
            RSSFeed(title = "Foo", description = "Bar", link = "https://baz.com/"),
            Feed.parse(string)
        )
    }
}